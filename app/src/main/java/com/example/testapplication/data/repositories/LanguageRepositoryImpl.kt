package com.example.testapplication.data.repositories

import com.example.testapplication.data.model.ResponseNetworkModel
import com.example.testapplication.data.network.ApiService
import com.example.testapplication.domain.repositories.LanguageRepository
import io.reactivex.Single

class LanguageRepositoryImpl(private val service: ApiService) : LanguageRepository {
    override fun getLanguage(word: String): Single<ResponseNetworkModel> {
        return service.getLanguage(word)
    }
}
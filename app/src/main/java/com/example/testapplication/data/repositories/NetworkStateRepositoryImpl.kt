package com.example.testapplication.data.repositories

import android.content.Context
import android.net.ConnectivityManager
import com.example.testapplication.domain.repositories.NetworkStateRepository

class NetworkStateRepositoryImpl(private val context: Context) : NetworkStateRepository {
    override fun isOnline(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            ?: return false

        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }
}
package com.example.testapplication.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.testapplication.data.model.WordDBModel

@Database(entities = [WordDBModel::class], version = 1)
abstract class DataBase : RoomDatabase() {
    abstract fun wordDao(): WordDao
}
package com.example.testapplication.data

import com.example.testapplication.data.model.WordDBModel
import com.example.testapplication.domain.entities.Word

fun WordDBModel.toWord() =
    Word(
        value = this.word,
        language = this.language
    )

fun Word.toWordDBModel() =
    WordDBModel(
        word = this.value,
        language = this.language
    )
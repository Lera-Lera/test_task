package com.example.testapplication.data.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.testapplication.data.model.WordDBModel

@Dao
interface WordDao {
    @Query("SELECT * FROM worddbmodel")
    fun getAllWords(): List<WordDBModel>

    @Insert
    fun insertNewWord(word: WordDBModel)
}
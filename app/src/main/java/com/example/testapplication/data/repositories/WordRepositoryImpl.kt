package com.example.testapplication.data.repositories

import com.example.testapplication.data.database.DataBase
import com.example.testapplication.data.toWordDBModel
import com.example.testapplication.domain.entities.Word
import com.example.testapplication.domain.repositories.WordRepository
import io.reactivex.Completable

class WordRepositoryImpl(
    dataBase: DataBase
) : WordRepository {
    private val wordDao = dataBase.wordDao()

    override fun insertWord(word: String, language: String): Completable {
        return Completable.fromAction { wordDao.insertNewWord(Word(word, language).toWordDBModel()) }
    }
}
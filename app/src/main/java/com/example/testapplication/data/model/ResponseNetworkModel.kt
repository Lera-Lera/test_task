package com.example.testapplication.data.model

data class ResponseNetworkModel(
    val languages: List<Language>
)

data class Language(
    val language: String
)
package com.example.testapplication.data.repositories

import com.example.testapplication.data.database.DataBase
import com.example.testapplication.data.toWord
import com.example.testapplication.domain.entities.Word
import com.example.testapplication.domain.repositories.HistoryRepository
import io.reactivex.Single

class HistoryRepositoryImpl(dataBase: DataBase) : HistoryRepository {

    private val wordDao = dataBase.wordDao()

    override fun getAllWords(): Single<List<Word>> =
        Single.fromCallable { wordDao.getAllWords() }
            .map { list -> list.map { it.toWord() } }
}
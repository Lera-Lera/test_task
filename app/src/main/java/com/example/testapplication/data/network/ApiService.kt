package com.example.testapplication.data.network

import com.example.testapplication.data.model.ResponseNetworkModel
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {

    @Headers("Content-Type: text/plain")
    @POST("v3/identify")
    fun getLanguage(
        @Body word: String,
        @Query("version") version: String = "2018-05-01"
    ): Single<ResponseNetworkModel>
}

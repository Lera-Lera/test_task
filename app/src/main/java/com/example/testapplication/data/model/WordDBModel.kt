package com.example.testapplication.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class WordDBModel(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val word: String,
    val language: String
)
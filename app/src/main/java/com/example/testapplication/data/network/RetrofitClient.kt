package com.example.testapplication.data.network


import android.util.Log
import okhttp3.Credentials
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient

object RetrofitClient {
    private var _instance: Retrofit? = null

    val instance: Retrofit
        get() = createRetrofit()

    private fun createRetrofit(): Retrofit {
        if (_instance == null) {

            val logging = HttpLoggingInterceptor { Log.i("HTTP", it) }
            logging.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .authenticator { _, response ->
                    val request = response.request()
                    if (request.header("Authorization") != null) {
                        return@authenticator null
                    }
                    return@authenticator request.newBuilder()
                        .header(
                            "Authorization",
                            Credentials.basic("6987a48d-342e-4a69-8adc-e65b1cc0b9da", "MxYSIi6nQP2Y")
                        )
                        .build()
                }
                .build()

            _instance = Retrofit.Builder()
                .baseUrl("https://gateway.watsonplatform.net/language-translator/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
        }
        return _instance!!
    }
}
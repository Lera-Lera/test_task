package com.example.testapplication.domain.repositories

interface NetworkStateRepository {

    fun isOnline(): Boolean

}
package com.example.testapplication.domain.repositories

import com.example.testapplication.data.model.ResponseNetworkModel
import io.reactivex.Single

interface LanguageRepository {

    fun getLanguage(word: String): Single<ResponseNetworkModel>

}
package com.example.testapplication.domain.entities

import java.io.Serializable

data class Word(
    val value: String,
    val language: String
) : Serializable
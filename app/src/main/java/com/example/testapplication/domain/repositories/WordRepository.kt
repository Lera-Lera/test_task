package com.example.testapplication.domain.repositories

import io.reactivex.Completable

interface WordRepository {

    fun insertWord(word: String, language: String): Completable

}
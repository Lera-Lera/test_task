package com.example.testapplication.domain.usecases

import com.example.testapplication.domain.entities.Word
import com.example.testapplication.domain.repositories.HistoryRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

interface GetWordListUseCase {
    operator fun invoke(): Single<List<Word>>
}

class GetWordListUseCaseImplementation(private val historyRepository: HistoryRepository) : GetWordListUseCase {
    override fun invoke(): Single<List<Word>> =
        historyRepository.getAllWords()
            .subscribeOn(Schedulers.io())
}
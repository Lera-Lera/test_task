package com.example.testapplication.domain.repositories

import com.example.testapplication.domain.entities.Word
import io.reactivex.Single

interface HistoryRepository {

    fun getAllWords(): Single<List<Word>>

}
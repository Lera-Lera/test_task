package com.example.testapplication.domain.usecases

import com.example.testapplication.domain.repositories.NetworkStateRepository

interface NetworkStateUseCase {
    operator fun invoke(): Boolean
}

class NetworkStateUseCaseImplementation(
    private val networkStateRepository: NetworkStateRepository
) : NetworkStateUseCase {
    override fun invoke(): Boolean {
        return networkStateRepository.isOnline()
    }
}
package com.example.testapplication.domain.usecases

import com.example.testapplication.domain.repositories.LanguageRepository
import com.example.testapplication.domain.repositories.WordRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

interface GetLanguageOfWordUseCase {
    operator fun invoke(word: String): Single<String>
}

class GetLanguageOfWordUseCaseImplementation(
    private val wordRepository: WordRepository,
    private val languageRepository: LanguageRepository
) : GetLanguageOfWordUseCase {

    override fun invoke(word: String): Single<String> {
        return languageRepository
            .getLanguage(word)
            .map { it.languages.first().language }
            .doOnSuccess {
                wordRepository.insertWord(word, it)
                    .subscribeOn(Schedulers.io())
                    .subscribe()
            }
            .subscribeOn(Schedulers.io())
    }
}

package com.example.testapplication.presentation.newtext

import android.arch.persistence.room.Room
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.testapplication.R
import com.example.testapplication.data.database.DataBase
import com.example.testapplication.data.network.ApiService
import com.example.testapplication.data.network.RetrofitClient
import com.example.testapplication.data.repositories.LanguageRepositoryImpl
import com.example.testapplication.data.repositories.NetworkStateRepositoryImpl
import com.example.testapplication.data.repositories.WordRepositoryImpl
import com.example.testapplication.domain.usecases.GetLanguageOfWordUseCaseImplementation
import com.example.testapplication.domain.usecases.NetworkStateUseCaseImplementation
import com.example.testapplication.presentation.dialog.DialogFragment
import kotlinx.android.synthetic.main.fragment_new_text.*

class NewTextFragment : MvpAppCompatFragment(), NewTextView {

    @InjectPresenter
    lateinit var presenter: NewTextPresenter

    @ProvidePresenter
    fun providePresenter(): NewTextPresenter =
        NewTextPresenter(
            GetLanguageOfWordUseCaseImplementation(
                WordRepositoryImpl(
                    Room.databaseBuilder(requireContext(), DataBase::class.java, "database").build()
                ),
                LanguageRepositoryImpl(
                    RetrofitClient.instance.create(ApiService::class.java)
                )
            ),
            NetworkStateUseCaseImplementation(
                NetworkStateRepositoryImpl(requireContext())
            )
        )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_new_text, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        val floatingButton = fab
        floatingButton.setOnClickListener {
            val newText = new_text_edit_text.text.toString()
            presenter.onFloatingButtonClicked(newText)
        }
    }

    override fun showValidationError() {
        Toast.makeText(requireContext(), R.string.hint, Toast.LENGTH_SHORT).show()
    }

    override fun showError() {
        Toast.makeText(requireContext(), R.string.error, Toast.LENGTH_SHORT).show()
    }

    override fun showNoNetwork() {
        Toast.makeText(requireContext(), R.string.no_network, Toast.LENGTH_SHORT).show()
    }

    override fun showDialog(word: String, language: String) {
        val fragmentManager = fragmentManager
        val dialog = DialogFragment()
        val bundle = Bundle()
        bundle.putString("text", word)
        bundle.putString("language", language)
        dialog.arguments = bundle
        dialog.show(fragmentManager, "dialog")
    }
}

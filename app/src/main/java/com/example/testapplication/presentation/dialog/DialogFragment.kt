package com.example.testapplication.presentation.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import com.example.testapplication.R
import kotlinx.android.synthetic.main.dialog.view.*

class DialogFragment : AppCompatDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.dialog, null)
        val dialog =  AlertDialog.Builder(requireContext())
            .setView(view)
            .create()

        val bundle = this.arguments
        val word = bundle?.getString("text", "")
        val language = bundle?.getString("language", "")
        view.word_text_view.text = word
        view.languages_text_view.text = language

        return dialog
    }
}
package com.example.testapplication.presentation.activity

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.example.testapplication.R
import com.example.testapplication.presentation.history.HistoryFragment
import com.example.testapplication.presentation.newtext.NewTextFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navigationView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        if (savedInstanceState == null) {
            navigationView.menu.performIdentifierAction(R.id.navigation_new_text, 0)
        }
    }

    private fun initViews() {
        val toolbar = toolbar_widget as Toolbar
        setSupportActionBar(toolbar)

        drawerLayout = drawer_layout
        navigationView = navigation_view
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        val fragmentClass = when (item.itemId) {
            R.id.navigation_new_text -> NewTextFragment::class.java
            R.id.navigation_history -> HistoryFragment::class.java
            else -> NewTextFragment::class.java
        }

        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit()
        }

        item.isChecked = true
        title = item.title
        drawerLayout.closeDrawers()

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

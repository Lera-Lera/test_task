package com.example.testapplication.presentation.history

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testapplication.R
import com.example.testapplication.domain.entities.Word
import kotlinx.android.synthetic.main.item_for_fragment_history.view.*

class HistoryWordListAdapter : RecyclerView.Adapter<HistoryWordListAdapter.WordListHolder>() {
    private val wordList = mutableListOf<Word>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordListHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_for_fragment_history,
            parent,
            false
        )

        return WordListHolder(view)
    }

    override fun getItemCount(): Int {
        return wordList.size
    }

    fun refreshList(list: List<Word>) {
        this.wordList.clear()
        this.wordList.addAll(list)

        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: WordListHolder, position: Int) {
        holder.bind(wordList[position])
    }

    class WordListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(word: Word) {
            itemView.word_text_view.text = word.value
            itemView.languages_text_view.text = word.language
        }
    }
}
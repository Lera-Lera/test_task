package com.example.testapplication.presentation.newtext

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface NewTextView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showDialog(word: String, language: String)

    @StateStrategyType(SingleStateStrategy::class)
    fun showValidationError()

    @StateStrategyType(SingleStateStrategy::class)
    fun showError()

    @StateStrategyType(SingleStateStrategy::class)
    fun showNoNetwork()
}
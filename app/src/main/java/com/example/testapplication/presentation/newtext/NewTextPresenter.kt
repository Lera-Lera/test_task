package com.example.testapplication.presentation.newtext

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.testapplication.domain.usecases.GetLanguageOfWordUseCase
import com.example.testapplication.domain.usecases.NetworkStateUseCase
import io.reactivex.disposables.CompositeDisposable

@InjectViewState
class NewTextPresenter(
    private val getLanguageOfWordUseCase: GetLanguageOfWordUseCase,
    private val networkStateUseCase: NetworkStateUseCase
) : MvpPresenter<NewTextView>() {
    private val compositeDisposable = CompositeDisposable()

    fun onFloatingButtonClicked(word: String) {
        if (word == "") {
            viewState.showValidationError()
            return
        }

        if (networkStateUseCase()) {
            getLanguageOfWordUseCase(word)
                .doOnSubscribe { compositeDisposable.add(it) }
                .subscribe({ viewState.showDialog(word, it) }, {viewState.showError()})
        } else {
            viewState.showNoNetwork()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
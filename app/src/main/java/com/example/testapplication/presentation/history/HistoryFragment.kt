package com.example.testapplication.presentation.history

import android.arch.persistence.room.Room
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.testapplication.R
import com.example.testapplication.data.database.DataBase
import com.example.testapplication.data.repositories.HistoryRepositoryImpl
import com.example.testapplication.domain.entities.Word
import com.example.testapplication.domain.usecases.GetWordListUseCaseImplementation
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : MvpAppCompatFragment(), HistoryView {
    @InjectPresenter
    lateinit var presenter: HistoryPresenter
    private lateinit var adapterHistory: HistoryWordListAdapter

    @ProvidePresenter
    fun providePresenter(): HistoryPresenter =
        HistoryPresenter(
            GetWordListUseCaseImplementation(
                HistoryRepositoryImpl(
                    Room.databaseBuilder(requireContext(), DataBase::class.java, "database").build()
                )
            )
        )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_history, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initViews() {
        adapterHistory = HistoryWordListAdapter()
        history_recycler_view.adapter = adapterHistory
    }

    override fun showWordList(wordList: List<Word>) {
        adapterHistory.refreshList(wordList)
    }

    override fun showError() {
        Toast.makeText(requireContext(), R.string.error, Toast.LENGTH_SHORT).show()
    }
}

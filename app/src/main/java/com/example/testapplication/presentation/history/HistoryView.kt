package com.example.testapplication.presentation.history

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.testapplication.domain.entities.Word

interface HistoryView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showWordList(wordList: List<Word>)

    @StateStrategyType(SingleStateStrategy::class)
    fun showError()
}